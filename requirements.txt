tensorflow==1.11.0
h5py==2.8.0

./thirdparty/donkey[tf,pi] ; platform_machine=='armv7l'
./thirdparty/donkey[tf] ; platform_machine!='armv7l'

docopt
xbox360controller==1.1.2
