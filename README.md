# GorillaCar

## Project setup cheat sheet

- Clone
- Pull in submodules.
  ```
  $ git submodule update --init --recursive
  ```
- Install dependencies from requirements.txt.
  ```
  $ virtualenv <some location>
  $ source <some location>/bin/activate
  $ pip install -r requirements.txt
  ```
