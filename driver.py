"""
GorillaCar driver service.

Usage:
    driver.py --tubfolder=<tubfolder>

Options:
    --tubfolder PATH   Folder where tubs will be stored.
"""

import math
from docopt import docopt

from donkeycar.parts.camera import PiCamera
from donkeycar.parts.actuator import PCA9685, PWMSteering, PWMThrottle
from donkeycar.parts.datastore import TubWriter
from donkeycar.parts.clock import Timestamp
from donkeycar.vehicle import Vehicle

from xbox360controller import Xbox360Controller

DRIVE_LOOP_HZ = 20

CAMERA_RESOLUTION = (120, 160)  # (height, width)

STEERING_CHANNEL = 1
STEERING_LEFT_PWM = 470
STEERING_RIGHT_PWM = 280

THROTTLE_CHANNEL = 0
THROTTLE_FORWARD_PWM = 360
THROTTLE_STOPPED_PWM = 390
THROTTLE_REVERSE_PWM = 420

JOYSTICK_MAX_THROTTLE = 1.0
AUTO_RECORD_ON_THROTTLE = True


class Xbox360ControllerPart(object):

    def __init__(self, max_throttle, auto_record_on_throttle):
        self.max_throttle = max_throttle
        self.auto_record_on_throttle = auto_record_on_throttle
        self.controller = None

    def read_controller_values(self):
        try:
            if self.controller == None:
                self.controller = Xbox360Controller(0)

            # I'm still not sure whether property access being atomic is a language guarantee,
            # or just an accident because of the GIL. Regardless: be mindful that reading 
            # these properties multiple times during a run will likely yield differing values.

            angle = self.apply_axis_threshold(self.controller.axis_r.x)
            throttle = self.apply_axis_threshold(self.controller.axis_l.y) * -1.0 * self.max_throttle

            return (angle, throttle)

        except Exception:
            # The controller library throws a generic exception if the device couldn't be accessed.
            self.controller = None
            return (0.0, 0.0)

    def apply_axis_threshold(self, value):
        # Even when completely released, axis controllers tend to output non-0 values.
        if abs(value) < 0.2:
            return 0.0

        return value

    def run(self):
        (angle, throttle) = self.read_controller_values()
        recording = self.auto_record_on_throttle and not math.isclose(throttle, 0.0)

        return angle, throttle, recording

    def shutdown(self):
        if self.controller != None:
            self.controller.close()
            self.controller = None


def construct_vehicle(tubfolder):
    V = Vehicle()

    clock = Timestamp()
    V.add(clock, outputs='timestamp')

    cam = PiCamera(resolution=CAMERA_RESOLUTION)
    V.add(cam, outputs=['cam/image_array'], threaded=True)

    ctr = Xbox360ControllerPart(max_throttle=JOYSTICK_MAX_THROTTLE,
                                auto_record_on_throttle=AUTO_RECORD_ON_THROTTLE)
    V.add(ctr,
          outputs=['user/angle', 'user/throttle', 'recording'])

    steering_controller = PCA9685(STEERING_CHANNEL)
    steering = PWMSteering(controller=steering_controller,
                           left_pulse=STEERING_LEFT_PWM,
                           right_pulse=STEERING_RIGHT_PWM)

    throttle_controller = PCA9685(THROTTLE_CHANNEL)
    throttle = PWMThrottle(controller=throttle_controller,
                           max_pulse=THROTTLE_FORWARD_PWM,
                           zero_pulse=THROTTLE_STOPPED_PWM,
                           min_pulse=THROTTLE_REVERSE_PWM)

    V.add(steering, inputs=['user/angle'])
    V.add(throttle, inputs=['user/throttle'])

    inputs = ['cam/image_array', 'user/angle', 'user/throttle', 'timestamp']
    types = ['image_array', 'float', 'float',  'str']

    tub = TubWriter(path=tubfolder, inputs=inputs, types=types)
    V.add(tub, inputs=inputs, run_condition='recording')

    return V


if __name__ == '__main__':
    args = docopt(__doc__)

    tubfolder = args['--tubfolder']

    V = construct_vehicle(tubfolder)
    V.start(rate_hz=DRIVE_LOOP_HZ)
